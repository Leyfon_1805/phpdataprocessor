## Build & run:
* mvn clean package
* java -jar target/technicalchallenge-1.0-SNAPSHOT.jar

## Running
 * Using http://localhost:8080/swagger-ui.html to interact with question api.
 * POST /question/SqlDump to upload mySql dump
 * GET /question/ to have all questions documents. It is possible to use question and anwser parameter to filter, only one filter per request. 
