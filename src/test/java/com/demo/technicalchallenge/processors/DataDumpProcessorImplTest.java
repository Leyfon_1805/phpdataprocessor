package com.demo.technicalchallenge.processors;

import com.demo.technicalchallenge.documents.QuestionAnwserDocument;
import com.demo.technicalchallenge.services.PhpContentParser;
import com.demo.technicalchallenge.services.PhpContentParserImpl;
import com.demo.technicalchallenge.services.SQLFileParser;
import com.demo.technicalchallenge.services.SQLFileParserImpl;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class DataDumpProcessorImplTest {
    private DataDumpProcessor processor;

    @Before
    public void setUp() throws Exception {
        SQLFileParser sqlFileParser = new SQLFileParserImpl();
        PhpContentParser phpContentParser = new PhpContentParserImpl();
        processor = new DataDumpProcessorImpl(phpContentParser, sqlFileParser);
    }

    @Test
    public void test_file_mvf_data_engineering_challenge() throws Exception {
        ClassLoader classLoader = getClass().getClassLoader();
        File sqlFile = new File(classLoader.getResource("mvf_data_engineering_challenge.sql").getFile());

        Set<QuestionAnwserDocument> estract = processor.estract(sqlFile);

        assertThat(estract).hasSize(10);
        estract.forEach(document -> {
            assertThat(document.hasAnwser()).isTrue();
            assertThat(document.hasQuestion()).isTrue();
        });
    }
}