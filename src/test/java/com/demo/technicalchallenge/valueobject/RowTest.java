package com.demo.technicalchallenge.valueobject;

import nz.net.osnz.common.pherialize.PhpSerializer;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class RowTest {
    @Test
    public void test_row_id_1() throws Exception {
        String testRow = "(1,'a:15:{s:11:\"description\";s:617:\"<strong>What is Critical Illness cover?</strong>\r\n<p>Personal Life Cover will pay out in event of your death during the policy term. Critical Illness cover will pay out in the event you are diagnosed with one of the illnesses defined by the policy.</p>\r\n<p><strong>Tip:</strong> Adding critical illness cover will typically make your policy more expensive therefore it is not for everyone. It is worth noting that on average our main partners pay out on 93% of claims where the policy holder has fallen critically ill*.</p>\r\n<p style=\"font-size:0.8em;\">* Statistics provided by the association of British Insurers</p>\";s:5:\"items\";s:122:\"personal_life_cover|Personal Life Cover\r\npersonal_life_cover_and_critical_illness|Personal Life Cover and Critical Illness\";s:8:\"multiple\";i:0;s:7:\"columns\";s:9:\"unlimited\";s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:6:\"aslist\";i:0;s:7:\"optrand\";i:0;s:12:\"other_option\";N;s:10:\"other_text\";s:8:\"Other...\";s:11:\"custom_keys\";b:0;s:14:\"options_source\";s:0:\"\";s:21:\"conditional_component\";s:0:\"\";s:20:\"conditional_operator\";s:1:\"=\";s:18:\"conditional_values\";s:0:\"\";}'),";
        Row row = new Row(testRow);

        String valueOfRow = row.getValueOfRow();

        assertThat(valueOfRow).isEqualTo("a:15:{s:11:\"description\";s:617:\"<strong>What is Critical Illness cover?</strong>\r\n<p>Personal Life Cover will pay out in event of your death during the policy term. Critical Illness cover will pay out in the event you are diagnosed with one of the illnesses defined by the policy.</p>\r\n<p><strong>Tip:</strong> Adding critical illness cover will typically make your policy more expensive therefore it is not for everyone. It is worth noting that on average our main partners pay out on 93% of claims where the policy holder has fallen critically ill*.</p>\r\n<p style=\"font-size:0.8em;\">* Statistics provided by the association of British Insurers</p>\";s:5:\"items\";s:122:\"personal_life_cover|Personal Life Cover\r\npersonal_life_cover_and_critical_illness|Personal Life Cover and Critical Illness\";s:8:\"multiple\";i:0;s:7:\"columns\";s:9:\"unlimited\";s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:6:\"aslist\";i:0;s:7:\"optrand\";i:0;s:12:\"other_option\";N;s:10:\"other_text\";s:8:\"Other...\";s:11:\"custom_keys\";b:0;s:14:\"options_source\";s:0:\"\";s:21:\"conditional_component\";s:0:\"\";s:20:\"conditional_operator\";s:1:\"=\";s:18:\"conditional_values\";s:0:\"\";}");
    }
}