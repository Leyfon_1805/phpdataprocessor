package com.demo.technicalchallenge.services;

import de.ailis.pherialize.exceptions.UnserializeException;
import nz.net.osnz.common.pherialize.PhpSerializer;
import org.junit.Before;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class PhpContentParserImplTest {

    private PhpContentParser parser;

    @Before
    public void setUp() throws Exception {
        parser = new PhpContentParserImpl();
    }

    @Test
    public void test_deserialization_success() throws Exception {
        String value = "a:15:{s:11:\"description\";s:617:\"<strong>What is Critical Illness cover?</strong>\r\n<p>Personal Life Cover will pay out in event of your death during the policy term. Critical Illness cover will pay out in the event you are diagnosed with one of the illnesses defined by the policy.</p>\r\n<p><strong>Tip:</strong> Adding critical illness cover will typically make your policy more expensive therefore it is not for everyone. It is worth noting that on average our main partners pay out on 93% of claims where the policy holder has fallen critically ill*.</p>\r\n<p style=\"font-size:0.8em;\">* Statistics provided by the association of British Insurers</p>\";s:5:\"items\";s:122:\"personal_life_cover|Personal Life Cover\r\npersonal_life_cover_and_critical_illness|Personal Life Cover and Critical Illness\";s:8:\"multiple\";i:0;s:7:\"columns\";s:9:\"unlimited\";s:13:\"title_display\";s:6:\"before\";s:7:\"private\";i:0;s:6:\"aslist\";i:0;s:7:\"optrand\";i:0;s:12:\"other_option\";N;s:10:\"other_text\";s:8:\"Other...\";s:11:\"custom_keys\";b:0;s:14:\"options_source\";s:0:\"\";s:21:\"conditional_component\";s:0:\"\";s:20:\"conditional_operator\";s:1:\"=\";s:18:\"conditional_values\";s:0:\"\";}";
        String expectedDesciption = "<strong>What is Critical Illness cover?</strong>\n" +
                "<p>Personal Life Cover will pay out in event of your death during the policy term. Critical Illness cover will pay out in the event you are diagnosed with one of the illnesses defined by the policy.</p>\n" +
                "<p><strong>Tip:</strong> Adding critical illness cover will typically make your policy more expensive therefore it is not for everyone. It is worth noting that on average our main partners pay out on 93% of claims where the policy holder has fallen critically ill*.</p>\n" +
                "<p style=\"font-size:0.8em;\">* Statistics provided by the association of British Insurers</p>";
        Map<Object, Object> deserializeValue = parser.deserialize(value);

        assertThat(deserializeValue).hasSize(15);
        String description = deserializeValue.get("description").toString();
        System.out.println(deserializeValue.get("items"));
        assertThat(description).isEqualToNormalizingNewlines(expectedDesciption);
    }
    
    @Test(expected = UnserializeException.class)
    public void test_deserialization_invalid_format() throws Exception {
        String value = "test_invalid_format";

        Map<Object, Object> deserializeValue = parser.deserialize(value);

        assertThat(deserializeValue).hasSize(15);
    }

    @Test
    public void test_remove_tags() throws Exception {
        String value = "<strong>What is Critical Illness cover?</strong>\r\n<p>Personal Life Cover will pay out in event of your death during the policy term. Critical Illness cover will pay out in the event you are diagnosed with one of the illnesses defined by the policy.</p>\r\n<p><strong>Tip:</strong> Adding critical illness cover will typically make your policy more expensive therefore it is not for everyone. It is worth noting that on average our main partners pay out on 93% of claims where the policy holder has fallen critically ill*.</p>\r\n<p style=\"font-size:0.8em;\">* Statistics provided by the association of British Insurers</p>";
        String expectedOutput = "What is Critical Illness cover? Personal Life Cover will pay out in event of your death during the policy term. Critical Illness cover will pay out in the event you are diagnosed with one of the illnesses defined by the policy. Tip: Adding critical illness cover will typically make your policy more expensive therefore it is not for everyone. It is worth noting that on average our main partners pay out on 93% of claims where the policy holder has fallen critically ill*. * Statistics provided by the association of British Insurers";

        String removedHtlmTags = parser.removeHtlmTags(value);

        assertThat(removedHtlmTags).isEqualTo(expectedOutput);
    }

    @Test
    public void test_remove_line_breaks() throws Exception {
        String value = "personal_life_cover|Personal Life Cover\n" +
                "personal_life_cover_and_critical_illness|Personal Life Cover and Critical Illness";
        String expectedOutput = "personal_life_cover|Personal Life Coverpersonal_life_cover_and_critical_illness|Personal Life Cover and Critical Illness";

        String removeLineBreaks = parser.removeLineBreaks(value);

        assertThat(removeLineBreaks).isEqualTo(expectedOutput);
    }
}