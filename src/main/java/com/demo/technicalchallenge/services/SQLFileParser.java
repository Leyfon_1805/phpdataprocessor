package com.demo.technicalchallenge.services;

import com.demo.technicalchallenge.valueobject.Row;

import java.io.File;
import java.util.List;

public interface SQLFileParser {
    List<Row> getValuesRow(File file);
}
