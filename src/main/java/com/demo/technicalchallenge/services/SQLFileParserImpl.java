package com.demo.technicalchallenge.services;

import com.demo.technicalchallenge.valueobject.Row;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class SQLFileParserImpl implements SQLFileParser {
    @Override
    public List<Row> getValuesRow(File file) {
        List<Row> values = Collections.emptyList();

        try (Stream<String> stream = Files.lines(Paths.get(file.getPath()))) {

            values = stream
                    .filter(line -> line.startsWith("\t("))
                    .map(line -> new Row(line))
                    .collect(Collectors.toList());


        } catch (IOException e) {
           throw new RuntimeException(e);
        }

        return values;
    }
}
