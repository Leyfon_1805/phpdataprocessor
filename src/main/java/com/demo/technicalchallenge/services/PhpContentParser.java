package com.demo.technicalchallenge.services;

import java.util.Map;

public interface PhpContentParser {
    Map<Object, Object> deserialize(String value);

    String removeHtlmTags(String value);

    String removeLineBreaks(String value);
}
