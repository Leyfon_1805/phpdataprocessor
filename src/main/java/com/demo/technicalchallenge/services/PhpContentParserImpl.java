package com.demo.technicalchallenge.services;

import de.ailis.pherialize.MixedArray;
import de.ailis.pherialize.Pherialize;
import org.jsoup.Jsoup;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class PhpContentParserImpl implements PhpContentParser {
    @Override
    public Map<Object, Object> deserialize(String value) {
        MixedArray map = Pherialize.unserialize(value).toArray();
        return map;
    }

    @Override
    public String removeHtlmTags(String value) {
        return Jsoup.parse(value).text();
    }

    @Override
    public String removeLineBreaks(String value) {
        return value.replace("\n", "").replace("\r", "");
    }
}
