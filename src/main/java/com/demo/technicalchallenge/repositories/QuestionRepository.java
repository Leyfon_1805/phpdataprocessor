package com.demo.technicalchallenge.repositories;

import com.demo.technicalchallenge.documents.QuestionAnwserDocument;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

public interface QuestionRepository extends ElasticsearchRepository<QuestionAnwserDocument, String> {
    List<QuestionAnwserDocument> findByQuestionLike(String question);

    List<QuestionAnwserDocument> findByAnwserLike(String anwser);
}
