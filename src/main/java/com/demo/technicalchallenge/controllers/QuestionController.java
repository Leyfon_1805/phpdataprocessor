package com.demo.technicalchallenge.controllers;

import com.demo.technicalchallenge.documents.QuestionAnwserDocument;
import com.demo.technicalchallenge.processors.DataDumpProcessor;
import com.demo.technicalchallenge.repositories.QuestionRepository;
import com.google.common.collect.Lists;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping(value = "/question")
public class QuestionController {

    private static final String PREFIX = "TEMP";
    private static final java.lang.String SUFFIX = ".tmp";
    private final QuestionRepository questionRepository;
    private final DataDumpProcessor dataDumpProcessor;

    public QuestionController(QuestionRepository questionRepository, DataDumpProcessor dataDumpProcessor) {
        this.questionRepository = questionRepository;
        this.dataDumpProcessor = dataDumpProcessor;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void postQuestion(@RequestBody QuestionAnwserDocument document) {
        questionRepository.save(document);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<QuestionAnwserDocument> getQuestions(@RequestParam(value = "question", required = false) String question,
                                                     @RequestParam(value = "anwser", required = false) String anwser) {
        if (question != null) {
            return questionRepository.findByQuestionLike(question);
        } else if (anwser != null) {
            return questionRepository.findByAnwserLike(anwser);
        } else {
            return Lists.newArrayList(questionRepository.findAll());
        }
    }

    @RequestMapping(value = "/SqlDump", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void sqlDump(@RequestParam("sql") MultipartFile sql) throws IOException {
        File tempFile = File.createTempFile(PREFIX, SUFFIX);
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(sql.getInputStream(), out);
        }
        Set<QuestionAnwserDocument> estractDocuments = dataDumpProcessor.estract(tempFile);
        questionRepository.save(estractDocuments);
    }

}
