package com.demo.technicalchallenge.documents;


import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.HashSet;
import java.util.Set;

@Document(indexName = "question_index", type = "questions")
public class QuestionAnwserDocument {
    @Id
    private final String question;
    private final Set<String> anwser;

    @Deprecated
    /**
     * Necessary for jackson
     */
    private QuestionAnwserDocument() {
        question = null;
        anwser = new HashSet<>();
    }

    public QuestionAnwserDocument(String question, Set<String> anwser) {
        this.question = question;
        this.anwser = anwser;
    }

    public String getQuestion() {
        return question;
    }

    public Set<String> getAnwser() {
        return anwser;
    }

    public boolean hasAnwser(){
        return !anwser.isEmpty();
    }

    public boolean hasQuestion(){
        return question.trim().length()>1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        QuestionAnwserDocument that = (QuestionAnwserDocument) o;

        return question != null ? question.equals(that.question) : that.question == null;
    }

    @Override
    public int hashCode() {
        return question != null ? question.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "QuestionAnwserDocument{" +
                "question='" + question + '\'' +
                ", anwser=" + anwser +
                '}';
    }
}
