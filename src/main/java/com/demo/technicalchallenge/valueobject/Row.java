package com.demo.technicalchallenge.valueobject;

import com.demo.technicalchallenge.documents.QuestionAnwserDocument;
import nz.net.osnz.common.pherialize.PhpSerializer;

public class Row {
    public static final String END_CHARS = "')";
    public static final String START_CHARS = ",'";
    private final String row;

    public Row(String row) {
        this.row = row;
    }

    public String getRow() {
        return row;
    }

    public String getValueOfRow() {
        int start = row.indexOf(START_CHARS) + START_CHARS.length();
        int end = row.lastIndexOf(END_CHARS);
        return row.substring(start, end);
    }

//    public QuestionAnwserDocument getDocument(){
//        String value = getValueOfRow();
//        value.contains()
//}

//    private String extractQeustion(){
//        PhpSerializer.
//    }
}
