package com.demo.technicalchallenge.processors;

import com.demo.technicalchallenge.documents.QuestionAnwserDocument;

import java.io.File;
import java.util.Map;
import java.util.Set;

public interface DataDumpProcessor {

    Set<QuestionAnwserDocument> estract(File file);

}
