package com.demo.technicalchallenge.processors;

import com.demo.technicalchallenge.documents.QuestionAnwserDocument;
import com.demo.technicalchallenge.services.PhpContentParser;
import com.demo.technicalchallenge.services.SQLFileParser;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class DataDumpProcessorImpl implements DataDumpProcessor {
    private final PhpContentParser phpContentParser;
    private final SQLFileParser sqlFileParser;

    public DataDumpProcessorImpl(PhpContentParser phpContentParser, SQLFileParser sqlFileParser) {
        this.phpContentParser = phpContentParser;
        this.sqlFileParser = sqlFileParser;
    }

    public Set<QuestionAnwserDocument> estract(File file) {
        return sqlFileParser.getValuesRow(file)
                .stream()
                .map(row -> createNewDocument(row.getValueOfRow()))
                .filter(QuestionAnwserDocument::hasAnwser)
                .filter(QuestionAnwserDocument::hasQuestion)
                .collect(Collectors.toSet());
    }

    private QuestionAnwserDocument createNewDocument(String rowValue){
        String normalizedValue = rowValue
                .replace("\\\"", "\"")
                .replace("\\r\\n", "\r\n")
                .replace("\\'", "\'");
        Map<Object, Object> valuesMap = phpContentParser.deserialize(normalizedValue);
        String question = normalizeQuestion(String.valueOf(valuesMap.get("description").toString()));
        Set<String> answer = normalizeAnswer(String.valueOf(valuesMap.get("items")));
        return new QuestionAnwserDocument(question, answer);
    }

    private String normalizeQuestion(String question){
        return phpContentParser.removeHtlmTags(question);
    }

    private Set<String> normalizeAnswer(String answers){
        String removeHtlmTags = phpContentParser.removeHtlmTags(answers);
        return Stream.of(answers.split("\n"))
                .filter(v -> v.contains("|"))
                .map(v -> {
                    int start = v.indexOf("|") + 1;
                    return v.substring(start);
                })
                .map(v -> phpContentParser.removeLineBreaks(v))
                .distinct()
                .collect(Collectors.toSet());
    }
}
